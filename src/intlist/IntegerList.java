package intlist;

import except.*;

public class IntegerList
{

    private Nut headNut;
    private int len = 0;

    public IntegerList()
    {
        return;
    }

    public void add(int num)
    {

        if (this.len == 0)
        {
            this.headNut = new Nut(null, num, null);
            this.len++;
            return;
        }

        Nut newNut = new Nut(null, num, null);
        Nut tempNut = findLastNode(this.headNut);
        tempNut.nextNut = newNut;
        newNut.prevNut = tempNut;
        this.len++;

        this.headNut = headReset(this.headNut);
    }

    private Nut findLastNode(Nut startNut)
    {
        if (startNut.nextNut == null)
        {
            return startNut;
        }

        return findLastNode(startNut.nextNut);
    }

    public void add(int pos, int num) throws OutOfBoundsException
    {
        Nut beforeNut;
        Nut afterNut;

        if (pos >= this.len || pos < 0)
        {
            throw new OutOfBoundsException(pos, this.len);
        }

        Nut tempNut = findNthNode(this.headNut, pos);

        beforeNut = tempNut.prevNut;
        afterNut = tempNut;
        Nut newNut = new Nut(beforeNut, num, afterNut);

        if (beforeNut != null)
        {
            beforeNut.nextNut = newNut;
        }

        if (afterNut != null)
        {
            afterNut.prevNut = newNut;
        }
        this.len++;

        this.headNut = headReset(this.headNut);

    }

    private Nut findNthNode(Nut startNut, int n)
    {
        if (n == 0)
        {
            return startNut;
        }

        return findNthNode(startNut.nextNut, n - 1);
    }

    private Nut headReset(Nut thisNut)
    {
        if (thisNut.prevNut == null)
        {
            return thisNut;
        }

        return headReset(thisNut.prevNut);
    }


    public void remove(int pos) throws OutOfBoundsException
    {
        Nut beforeNut;
        Nut afterNut;

        if (pos >= this.len || pos < 0)
        {
            throw new OutOfBoundsException(pos, this.len);
        }

        Nut tempNut = findNthNode(this.headNut, pos);

        beforeNut = tempNut.prevNut;
        afterNut = tempNut.nextNut;

        if (pos == 0)
        {
            this.headNut = headNut.nextNut;
        }

        if (beforeNut != null)
        {
            beforeNut.nextNut = afterNut;
        }

        if (afterNut != null)
        {
            afterNut.prevNut = beforeNut;
        }

        // this.headNut = headReset(this.headNut);
        this.len--;
        return;
    }

    public void sort()
    {
        IntegerList sortedList = new IntegerList();
        int listlen = this.len;

        for (int i = 0; i < listlen; i++)
        {
            int minPos = findMinPos(this.headNut);
            sortedList.add(valueAtPos(minPos, this.headNut));
            try{
            this.remove(minPos);
            }catch (OutOfBoundsException e)
            {}
        }

        this.headNut = sortedList.headNut;
    }

    private int findMinPos(Nut startNut)
    {
        int minVal = Integer.MAX_VALUE;
        int minPos = 0;

        for (int i = 0; i < this.len; i++)
        {
            int tempVal = this.valueAtPos(i, this.headNut);
            if (tempVal < minVal)
            {
                minPos = i;
                minVal = tempVal;
            }
        }
        // System.out.println("Found: minPos: " + minPos + " val: " + this.valueAtPos(minPos, this.headNut));
        return minPos;
    }

    private int valueAtPos(int pos, Nut startNut)
    {
        if (pos == 0)
        {
            return startNut.value;
        }

        return valueAtPos(--pos, startNut.nextNut);
    }

    public int size()
    {
        return this.len;
    }

    public void printList()
    {
        recPrint(this.headNut);
        System.out.println("");
        return;
    }

    public void recPrint(Nut thisNut)
    {

        System.out.print(thisNut.value); System.out.print(" ");

        if (thisNut.nextNut == null)
        {
            return;
        }

        recPrint(thisNut.nextNut);
    }
}

class Nut
{
    public Nut prevNut;
    public int value;
    public Nut nextNut;

    public Nut(Nut prev, int i_val, Nut next)
    {
        this.prevNut = prev;
        this.value = i_val;
        this.nextNut = next;
    }
}
