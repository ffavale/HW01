package except;

public class OutOfBoundsException extends Exception
{
    private static final long serialVersionUID = 0;

    private int idx;
    private int siz;

    public OutOfBoundsException(int i_idx ,int i_siz)
    {
        this.idx = i_idx;
        this.siz = i_siz;
    }

    public void printException()
    {
        System.out.print("OutOfBoundsException: ");
        System.out.print("Index: "); System.out.print(this.idx); System.out.print(' ');
        System.out.print("Size: "); System.out.println(this.siz);
        return;
    }
}
